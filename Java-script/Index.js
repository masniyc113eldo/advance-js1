
/* Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль. */

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    };

    get workerName() {
        return this._name;
    };

    set workerName(value) {
        this._name = value;
    };

    get workerAge() {
        return this._age;
    };

    set workerAge(value) {
        if (value < 18) {
            console.log("it's a pupil")
        }
        this._age = value;
    };

    get workerSalary() {
        return this._salary;
    };
    
};

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get workerSalary() {
        
        return super.workerSalary * 3;
    }

    get workerName() {
        return super.workerName + "Kitty";
    };

    get workerAge() {
        return super.workerAge;
    };

}

const bob = new Programmer ("Bob", 25, 3000, "Python");
const bob1 = new Programmer ("Robert", 16, 3000, "JS");
const bob2 = new Programmer ("Cleo", 32, 1000, "Java");
console.log(bob);
console.log(bob.workerSalary, bob.workerAge);
console.log(bob1.workerSalary, bob1.workerAge);
console.log(bob2.workerSalary, bob2.workerAge);